package com.example.playerm3.data

enum class PlayerStatus {
    LOADING, PLAYING, PAUSED, IDLE, ERROR
}