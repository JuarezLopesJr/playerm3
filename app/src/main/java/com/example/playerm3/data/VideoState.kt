package com.example.playerm3.data

data class VideoState(
    val playerStatus: PlayerStatus = PlayerStatus.LOADING
)