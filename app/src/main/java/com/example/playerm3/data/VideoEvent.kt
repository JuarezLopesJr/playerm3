package com.example.playerm3.data

sealed class VideoEvent {
    object VideoLoaded : VideoEvent()

    object ToggleStatus : VideoEvent()

    object VideoError : VideoEvent()
}