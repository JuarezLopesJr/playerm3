package com.example.playerm3.viewmodel

import androidx.lifecycle.ViewModel
import com.example.playerm3.data.PlayerStatus
import com.example.playerm3.data.VideoEvent
import com.example.playerm3.data.VideoState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class VideoViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(VideoState())

    val uiState = _uiState.asStateFlow()

    private fun togglePlayerStatus() {
        val playerStatus = _uiState.value.playerStatus

        val newPlayerStatus = if (playerStatus != PlayerStatus.PLAYING) {
            PlayerStatus.PLAYING
        } else {
            PlayerStatus.PAUSED
        }

        _uiState.value = _uiState.value.copy(
            playerStatus = newPlayerStatus
        )
    }

    fun handleEvent(videoEvent: VideoEvent) {
        when (videoEvent) {
            /* finished loading and it's ready to start playing,
              IDLE because it awaits user action */
            VideoEvent.VideoLoaded -> {
                _uiState.value = _uiState.value.copy(
                    playerStatus = PlayerStatus.IDLE
                )
            }

            VideoEvent.ToggleStatus -> togglePlayerStatus()

            VideoEvent.VideoError -> {
                _uiState.value = _uiState.value.copy(
                    playerStatus = PlayerStatus.ERROR
                )
            }
        }
    }
}