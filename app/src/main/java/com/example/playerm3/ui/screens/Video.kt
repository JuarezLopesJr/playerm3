@file:OptIn(ExperimentalLifecycleComposeApi::class)

package com.example.playerm3.ui.screens

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.playerm3.viewmodel.VideoViewModel

@Composable
fun VideoScreen(
    modifier: Modifier = Modifier,
    lifeCycleOwner: LifecycleOwner = LocalLifecycleOwner.current
) {
    val viewModel = viewModel<VideoViewModel>()

    val state by viewModel.uiState.collectAsStateWithLifecycle()

    VideoPlayer(
        modifier = modifier,
        lifeCycleOwner = lifeCycleOwner,
        videoState = state,
        handleEvent = viewModel::handleEvent
    )
}