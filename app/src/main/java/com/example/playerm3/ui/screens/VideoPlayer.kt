package com.example.playerm3.ui.screens

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.LifecycleOwner
import com.example.playerm3.R
import com.example.playerm3.data.PlayerStatus
import com.example.playerm3.data.VideoEvent
import com.example.playerm3.data.VideoState
import com.example.playerm3.utils.Tag.TAG_VIDEO_PLAYER
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player

@Composable
fun VideoPlayer(
    modifier: Modifier = Modifier,
    lifeCycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
    videoState: VideoState,
    handleEvent: (VideoEvent) -> Unit
) {
    val context = LocalContext.current

    val media = MediaItem.fromUri(
        "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
    )

    val exoPlayer = remember {
        ExoPlayer.Builder(context).build().apply {
            setMediaItem(media)
            addListener(
                object : Player.Listener {
                    override fun onPlaybackStateChanged(playbackState: Int) {
                        super.onPlaybackStateChanged(playbackState)
                        if (playbackState == Player.STATE_READY) {
                            handleEvent(VideoEvent.VideoLoaded)
                        }
                    }
                }
            )
        }
    }

    Box(modifier = modifier.background(Color.Black)) {
        var controlsVisible by remember { mutableStateOf(true) }

        val controlsClickLabel = if (controlsVisible) {
            stringResource(id = R.string.label_hide_controls)
        } else {
            stringResource(id = R.string.label_display_controls)
        }

        val alphaAnimation by animateFloatAsState(
            targetValue = if (controlsVisible) 0.7f else 0f,
            animationSpec = if (controlsVisible) {
                tween()
            } else {
                tween(delayMillis = 750)
            }
        )

        Playback(
            modifier = Modifier
                .testTag(TAG_VIDEO_PLAYER)
                .fillMaxSize()
                .clickable(onClickLabel = controlsClickLabel) {
                    controlsVisible = !controlsVisible
                },
            exoPlayer = exoPlayer,
            context = context,
            lifeCycleOwner = lifeCycleOwner,
            state = videoState.playerStatus
        )

        Controls(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
                .alpha(alphaAnimation),
            playerStatus = videoState.playerStatus,
            togglePlayingState = {
                handleEvent(VideoEvent.ToggleStatus)

                if (videoState.playerStatus != PlayerStatus.PLAYING) {
                    controlsVisible = false
                }
            }
        )
    }
}