package com.example.playerm3.ui.screens

import android.content.Context
import android.view.ViewGroup
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.example.playerm3.data.PlayerStatus
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ui.StyledPlayerView

@Composable
fun Playback(
    modifier: Modifier = Modifier,
    lifeCycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
    state: PlayerStatus = PlayerStatus.IDLE,
    exoPlayer: ExoPlayer,
    context: Context
) {
    val currentPlayerStatus by rememberUpdatedState(state)

    /* running prepare() only in the initial composition */
    LaunchedEffect(key1 = exoPlayer) {
        exoPlayer.prepare()
    }

    /* checking the lifecycle to avoid memory leak */
    DisposableEffect(lifeCycleOwner) {
        val observer = LifecycleEventObserver { _, event ->
            if (currentPlayerStatus == PlayerStatus.PLAYING) {
                if (event == Lifecycle.Event.ON_RESUME) {
                    exoPlayer.play()
                } else if (event == Lifecycle.Event.ON_PAUSE) {
                    exoPlayer.pause()
                }
            }
        }

        lifeCycleOwner.lifecycle.addObserver(observer)

        onDispose {
            lifeCycleOwner.lifecycle.removeObserver(observer)
        }
    }

    DisposableEffect(
        AndroidView(
            modifier = modifier,
            factory = {
                StyledPlayerView(context).apply {
                    layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                    hideController()
                    useController = false
                    player = exoPlayer
                }
            },
            update = {
                when (state) {
                    PlayerStatus.PLAYING -> exoPlayer.play()
                    PlayerStatus.PAUSED -> exoPlayer.pause()
                    PlayerStatus.LOADING -> {}
                    PlayerStatus.IDLE -> {}
                    PlayerStatus.ERROR -> {}
                }
            }
        )
    ) {
        onDispose { exoPlayer.release() }
    }
}