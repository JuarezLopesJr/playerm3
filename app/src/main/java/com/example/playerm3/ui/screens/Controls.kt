package com.example.playerm3.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Pause
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import com.example.playerm3.R
import com.example.playerm3.data.PlayerStatus
import com.example.playerm3.utils.Tag.TAG_CONTROLS
import com.example.playerm3.utils.Tag.TAG_CONTROL_BUTTON

@Composable
fun Controls(
    modifier: Modifier = Modifier,
    playerStatus: PlayerStatus,
    togglePlayingState: () -> Unit
) {

    val playerIcon = if (playerStatus == PlayerStatus.PLAYING) {
        Icons.Default.Pause
    } else {
        Icons.Default.PlayArrow
    }

    val description = if (playerStatus == PlayerStatus.PLAYING) {
        stringResource(id = R.string.cd_pause)
    } else {
        stringResource(id = R.string.cd_play)
    }

    Box(
        modifier = modifier
            .testTag(TAG_CONTROLS)
            .background(MaterialTheme.colorScheme.surface)
            .fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        IconButton(
            modifier = Modifier.testTag(TAG_CONTROL_BUTTON),
            onClick = { togglePlayingState() },
            enabled = playerStatus != PlayerStatus.LOADING
        ) {
            Icon(imageVector = playerIcon, contentDescription = description)
        }
    }
}