package com.example.playerm3.utils

object Tag {
    const val TAG_CONTROL_BUTTON = "control_button"
    const val TAG_VIDEO_PLAYER = "video_player"
    const val TAG_CONTROLS = "controls"
}