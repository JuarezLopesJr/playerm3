package com.example.playerm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsEnabled
import androidx.compose.ui.test.assertIsNotEnabled
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.playerm3.data.PlayerStatus
import com.example.playerm3.ui.screens.Controls
import com.example.playerm3.utils.Tag.TAG_CONTROL_BUTTON
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class ControlsTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext
            .getString(id)

    @Test
    fun assert_Player_Control_Not_Enabled_When_Video_Not_Ready() {
        composeTestRule.setContent {
            Controls(playerStatus = PlayerStatus.LOADING, togglePlayingState = {})
        }

        composeTestRule.onNodeWithTag(TAG_CONTROL_BUTTON).assertIsNotEnabled()
    }

    @Test
    fun assert_Player_Control_Enabled_When_Video_Ready() {
        composeTestRule.setContent {
            Controls(playerStatus = PlayerStatus.IDLE, togglePlayingState = {})
        }

        composeTestRule.onNodeWithTag(TAG_CONTROL_BUTTON).assertIsEnabled()
    }

    @Test
    fun assert_Pause_Control_Displayed_When_Playing() {
        composeTestRule.setContent {
            Controls(playerStatus = PlayerStatus.PLAYING, togglePlayingState = {})
        }
        /* using contentDescription to be able to test visual element */
        composeTestRule.onNodeWithContentDescription(
            getTestString(R.string.cd_pause)
        ).assertIsDisplayed()
    }

    @Test
    fun assert_Play_Control_Displayed_When_Paused() {
        composeTestRule.setContent {
            Controls(playerStatus = PlayerStatus.PAUSED, togglePlayingState = {})
        }
        /* using contentDescription to be able to test visual element */
        composeTestRule.onNodeWithContentDescription(
            getTestString(R.string.cd_play)
        ).assertIsDisplayed()
    }

    @Test
    fun assert_Toggle_Play_State_Triggered() {
        val togglePlayingState: () -> Unit = mock()

        composeTestRule.setContent {
            Controls(
                playerStatus = PlayerStatus.PAUSED,
                togglePlayingState = togglePlayingState
            )
        }

        composeTestRule.onNodeWithTag(TAG_CONTROL_BUTTON).performClick()

        verify(togglePlayingState).invoke()
    }
}