package com.example.playerm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import com.example.playerm3.ui.screens.VideoScreen
import com.example.playerm3.utils.Tag.TAG_CONTROLS
import com.example.playerm3.utils.Tag.TAG_VIDEO_PLAYER
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class VideoTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setComposeTestRule() {
        composeTestRule.setContent { VideoScreen() }
    }

    @Test
    fun assert_Player_Controls_Displayed_By_Default() {
        composeTestRule.onNodeWithTag(TAG_CONTROLS).assertIsDisplayed()
    }

    @Test
    fun assert_Video_Player_Is_Displayed() {
        composeTestRule.onNodeWithTag(TAG_VIDEO_PLAYER).assertIsDisplayed()
    }
}